
var movieApp = angular.module('movieApp', ['ngRoute']);

movieApp.controller('MovieDetailsController', function ($scope, $http) {
  $http.get('movies.json').success(function(data) {
    $scope.movies= data;
  });
}); 

var MovieApp = angular.module('MovieApp', []);

MovieApp.controller('MovieListCtrl', function ($scope) {
  $scope.movies = [
    {'name': 'Swami Ra Ra',
     'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ull amcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.',
	 'imageUrl':'images/movie/swamy-ra-ra1.jpg'},
    {'name': 'Autonagar Surya',
      'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ull amcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.',
	'imageUrl':'images/movie/ANS01.jpg'},
    {'name': 'Mirchi',
     'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ull amcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.',
	'imageUrl':'images/movie/mirchi01.jpg'},
    {'name': 'Ishq',
      'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ull amcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.',
	'imageUrl':'images/movie/Ishq01.jpg'},
    {'name': 'Nenokkadine',
     'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ull amcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.',
	'imageUrl':'images/movie/one01.jpg'},	 
    {'name': 'Race Gurram',
      'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ull amcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.',
	'imageUrl':'images/movie/race-gurram1.jpg'}	 
  ];
});

MovieApp.controller('MovieDetailsCtrl', function ($scope) {
  $scope.movies = [
    {'name': 'Swami Ra Ra',
     'desc': 'Swamy Ra Ra is a 2013 Telugu film directed by debutant Sudhir Varma and produced by Chakri Chirugupati under Lakshmi Narasimha Entertainments. It stars Nikhil Siddharth and Swati Reddy in the lead roles.',
	 'imageHero':'images/nikil.jpg',
	 'image1':'images/movie/thumbs/swamy-ra-ra1.jpg',
	 'image2':'images/movie/thumbs/swamy-ra-ra2.jpg',
	 'image3':'images/movie/thumbs/swamy-ra-ra3.jpg',
	 'image4':'images/movie/thumbs/swamy-ra-ra4.jpg',
	 'image5':'images/movie/thumbs/swamy-ra-ra5.jpg',
	 'image6':'images/movie/thumbs/swamy-ra-ra6.jpg'}	 
  ];
});

MovieApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/movies', {
        templateUrl: 'files/movie-list.html',
        controller: 'MovieListCtrl'
      }).
      when('/movies/:movieId', {
        templateUrl: 'partials/movie-detail.html',
        controller: 'MovieDetailCtrl'
      }).
      otherwise({
        redirectTo: '/movies'
      });
  }]);